package sv.iuh.fit.episode.Models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data@Builder
public class Episode implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String nameEpisode;
    private int episodeNo;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate releaseDate;
    private int timeDuration;
    @ManyToOne
    private Movie movie;

    public Episode(String nameEpisode, int episodeNo, LocalDate releaseDate, int timeDuration, Movie movie) {
        this.nameEpisode = nameEpisode;
        this.episodeNo = episodeNo;
        this.releaseDate = releaseDate;
        this.timeDuration = timeDuration;
        this.movie = movie;
    }

    public Episode(long id) {
        this.id = id;
    }
}
