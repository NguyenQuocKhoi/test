package sv.iuh.fit.comment.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import sv.iuh.fit.comment.models.Director;

public interface DirectorRepository extends JpaRepository<Director, Long> {
}
