package sv.iuh.fit.authjwt.repository;

import sv.iuh.fit.authjwt.entity.Token;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TokenRepository extends JpaRepository<Token, Long> {
    Token findByToken(String token);
}
